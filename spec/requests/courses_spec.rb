# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Courses', type: :request do
  describe 'POST /courses' do
    let(:course_attributes) { FactoryBot.attributes_for(:course) }

    let(:tutors_attributes) do
      tutors = []
      3.times do
        tutors << FactoryBot.attributes_for(:tutor)
      end
      tutors
    end

    context 'when course has no tutors params' do
      let(:params) { { course: { **course_attributes } } }
      it 'returns created course' do
        post(api_v1_courses_path, params:)
        expect(response).to have_http_status :created
      end
    end

    context 'when course has embedded tutor params' do
      let(:params) { { course: { **course_attributes, tutors_attributes: tutors_attributes} } }
      it 'returns created course' do
        post(api_v1_courses_path, params:)
        expect(response).to have_http_status :created
      end
    end

    context 'when new course has tutor from existing course' do
      let(:tutor_with_course) do
        course = FactoryBot.create(:course)
        tutor = FactoryBot.create(:tutor, course:)
        tutor.attributes.slice('first_name', 'last_name', 'email')
      end
      let(:params) do
        {
          course: {
            **course_attributes,
            tutors_attributes: tutors_attributes.push(tutor_with_course)
          }
        }
      end
      it 'returns status unprocessable entity' do
        post(api_v1_courses_path, params:)
        expect(response).to have_http_status :unprocessable_entity
      end
    end
  end

  describe 'GET /courses' do
    let!(:course) { FactoryBot.create(:course, :with_tutors) }

    it 'return tutors array embeded in courses' do
      get api_v1_courses_path
      response_body = JSON.parse(response.body)
      expect(response_body.first['tutors'].map(&:values))
        .to eq(course.tutors.pluck('first_name','last_name','email'))
    end
  end
end
