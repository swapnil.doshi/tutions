FactoryBot.define do
  factory :course do
    name { Faker::Educator.course_name}
    start_date { Date.today}
    end_date { Date.today + 45}

    trait :with_tutors do
      after(:create) do |c|
        FactoryBot.create_list(:tutor,3, course: c)
      end
    end
  end
end
