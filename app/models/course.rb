# frozen_string_literal: true

class Course < ApplicationRecord
  has_many :tutors

  validates_presence_of :name, :start_date, :end_date
  accepts_nested_attributes_for :tutors

  scope :with_tutors, -> { eager_load(:tutors) }
end
