# frozen_string_literal: true

class Tutor < ApplicationRecord
  belongs_to :course
  validates_presence_of :email, :first_name, :last_name
  validates_uniqueness_of :email
end
