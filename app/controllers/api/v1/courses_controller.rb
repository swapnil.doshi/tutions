# frozen_string_literal: true

module Api
  module V1
    class CoursesController < Api::V1::ApplicationController

      # GET /courses
      def index
        @courses = Course.with_tutors
        render json: @courses.as_json({ only: %i[id name],
                                        include: [tutors: { only: %i[first_name last_name email] }]
                                      })
      end

      # POST /courses
      def create
        @course = Course.new(course_params)

        if @course.save
          render json: @course, status: :created
        else
          render json: @course.errors, status: :unprocessable_entity
        end
      end

      private
      
      # Only allow a list of trusted parameters through.
      def course_params
        params.require(:course).permit(:name, :start_date, :end_date,
                                       tutors_attributes: %i[id email first_name last_name])
      end
    end
  end
end
